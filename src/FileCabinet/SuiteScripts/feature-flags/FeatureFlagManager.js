/**
 * FeatureFlagManager.js
 *
 * Provides methods for wrapping functions in a variety of feature flags.
 *
 * @module FeatureFlagManager
 *
 * @NApiVersion 2.1
 *
 * @author Eric T Grubaugh <eric@stoic.software>
 * @copyright 2022 Stoic Software, LLC
 * @license MIT
 */
define([
  'N/runtime',
  './FeatureFlagCore',
  './enums/FeatureFlagErrors',
  './enums/FeatureFlags'
], (
  runtime,
  FeatureFlagCore,
  FeatureFlagErrors,
  Flags
) => {
  /**
   * Returns a new function identical to the given function, except it will do nothing and return
   * undefined in any NetSuite Account not listed in the given accountIds
   *
   * @partial
   *
   * @gov 0
   *
   * @param {string[]} accountIds List of NetSuite Account IDs for which the wrapped function should
   *  execute; the function will not be invoked in any NetSuite Account not listed here. Account
   *  IDs are compared against `N/runtime.accountId`.
   * @param {function} fn The function to restrict to the given Account IDs
   *
   * @returns {function(*): *} a new function with the same behavior as the original, except it will
   *   do nothing and return undefined in any NetSuite Account not listed in the given accountIds
   *
   * @throws {FeatureFlagError} when the value given for accountIds is not a populated Array
   * @throws {FeatureFlagError} when the value given for fn is not a function
   */
  const forAccounts = FeatureFlagCore.forList(_validAccountIds, FeatureFlagErrors.INVALID_ACCOUNTS, _currentAccount);

  /**
   * Returns a new function identical to the given function, except it will do nothing and return
   * undefined for any Role not listed in the given roleIds
   *
   * @partial
   *
   * @gov 0
   *
   * @param {number[]} roleIds List of NetSuite Role IDs for which the wrapped function should
   *  execute; the function will not be invoked for any Role not listed here. Role IDs are compared
   *  against `N/runtime.getCurrentUser().role`.
   * @param {function} fn The function to restrict to the given Role IDs
   *
   * @returns {function(*): *} a new function with the same behavior as the original, except it will
   *   do nothing and return undefined for any Role not listed in the given roleIds
   *
   * @throws {FeatureFlagError} when the value given for roleIds is not a populated Array
   * @throws {FeatureFlagError} when the value given for fn is not a function
   */
  const forRoles = FeatureFlagCore.forList(_validRoleIds, FeatureFlagErrors.INVALID_ROLES, _currentRole);

  /**
   * Returns a new function identical to the given function, except it will do nothing and return
   * undefined for any User not listed in the given userIds
   *
   * @partial
   *
   * @gov 0
   *
   * @param {number[]} userIds List of NetSuite User IDs for which the wrapped function should
   *  execute; the function will not be invoked for any User not listed here. User IDs are compared
   *  against `N/runtime.getCurrentUser().id`. Many script types like Map/Reduce do not have an
   *  active User session, and thus the flagged function will not run in those script types.
   * @param {function} fn The function to restrict to the given User IDs
   *
   * @returns {function(*): *} a new function with the same behavior as the original, except it will
   *   do nothing and return undefined for any NetSuite User not listed in the given userIds
   *
   * @throws {FeatureFlagError} when the value given for userIds is not a populated Array
   * @throws {FeatureFlagError} when the value given for fn is not a function
   */
  const forUsers = FeatureFlagCore.forList(_validUserIds, FeatureFlagErrors.INVALID_USERS, _currentUser);

  /**
   * Returns a new function identical to the given function, except it will do nothing in a
   * Production environment.
   *
   * @partial
   *
   * @gov 0
   *
   * @param {function} fn The function to ignore in a Production environment but execute in all others
   *
   * @returns {function(*): *} a new function with the same behavior as the original, except it will
   *   do nothing and return undefined in a Production environment
   *
   * @throws {FeatureFlagError} when the value given for fn is not a function
   */
  const nonProduction = (fn) => {
    if (typeof fn !== 'function') throw FeatureFlagErrors.NOT_A_FUNCTION;

    return (...args) => {
      if (runtime.envType === runtime.EnvType.PRODUCTION) return;
      return fn(...args);
    };
  };

  /**
   * Returns a new function identical to the given function, except it will do nothing if the value
   * of the given Feature Flag is not truthy. Feature Flags are defined in
   * SuiteScripts/feature-flags/enums/FeatureFlags.js, which is an enumeration Object providing
   * key-value pairs of Feature Flags. Feature Flag names are the key names within the Object.
   * Values may be primitive booleans or getter functions which return a primitive boolean.
   *
   * @partial
   *
   * @gov 0
   *
   * @param {string} flag Flag name to search for within the Feature Flags enumeration
   * @param {function} fn The function to restrict by the given Feature Flag
   *
   * @returns {function(*): *} a new function with the same behavior as the original, except it will
   *   do nothing and return undefined when the corresponding Feature Flag is falsy
   *
   * @throws {FeatureFlagError} when flag is not a valid Feature Flag name
   * @throws {FeatureFlagError} when fn is not a function
   */
  const withFlag = FeatureFlagCore.forObject(Flags);

  /**
   * Retrieves the current NetSuite Account ID
   *
   * @gov 0
   *
   * @returns {string} the current NetSuite Account ID
   *
   * @private
   */
  function _currentAccount() {
    return runtime?.accountId;
  }

  /**
   * Retrieves the current Role ID
   *
   * @gov 0
   *
   * @returns {?number} the current Role ID
   *
   * @private
   */
  function _currentRole() {
    return runtime.getCurrentUser()?.role;
  }

  /**
   * Retrieves the current User ID
   *
   * @gov 0
   *
   * @returns {?number} the current User ID; can be undefined in certain script types that do not
   *  have a user session
   *
   * @private
   */
  function _currentUser() {
    return runtime.getCurrentUser()?.id;
  }

  /**
   * Determines whether the given list contains all numbers
   *
   * @gov 0
   *
   * @param {*[]} list The list to check
   *
   * @returns {boolean} true if the given list contains only numbers; false otherwise. Will return
   *  false for an empty list
   *
   * @private
   */
  function _isNumericArray(list) {
    return (
      Array.isArray(list) &&
      list.length &&
      list.every(id => typeof id === 'number')
    );
  }

  /**
   * Determines whether the given list of Account IDs is valid for use in a Feature Flag partial
   *
   * @gov 0
   *
   * @param {string[]} accountIds List of internal IDs of NetSuite Accounts to validate
   *
   * @returns {boolean} true if the given list is valid for use in a Feature Flag; false otherwise
   *
   * @private
   */
  function _validAccountIds(accountIds) {
    return (
      Array.isArray(accountIds) &&
      accountIds.length &&
      accountIds.every(id => typeof id === 'string')
    );
  }

  /**
   * Determines whether the given list of Role IDs is valid for use in a Feature Flag partial
   *
   * @gov 0
   *
   * @param {number[]} userIds List of internal IDs of NetSuite Roles to validate
   *
   * @returns {boolean} true if the given list is valid for use in a Feature Flag; false otherwise
   *
   * @private
   */
  function _validRoleIds(roleIds) {
    return _isNumericArray(roleIds);
  }

  /**
   * Determines whether the given list of User IDs is valid for use in a Feature Flag partial
   *
   * @gov 0
   *
   * @param {number[]} userIds List of internal IDs of NetSuite Users to validate
   *
   * @returns {boolean} true if the given list is valid for use in a Feature Flag; false otherwise
   *
   * @private
   */
  function _validUserIds(userIds) {
    return _isNumericArray(userIds);
  }

  return /** @alias module:FeatureFlagManager */ {
    forAccounts,
    forRoles,
    forUsers,
    nonProduction,
    withFlag
  };
});
