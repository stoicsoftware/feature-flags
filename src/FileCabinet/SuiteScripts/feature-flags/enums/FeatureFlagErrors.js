/**
 * FeatureFlagErrors.js
 *
 * Enumeration module for known errors thrown by the FeatureFlagManager
 *
 * @enum {FeatureFlagError} FeatureFlagErrors
 * @readonly
 *
 * @NApiVersion 2.1
 *
 * @author Eric T Grubaugh <eric@stoic.software>
 * @copyright 2022 Stoic Software, LLC
 * @license MIT
 */
define([], () => {
  const prefix = 'FFM_';
  return {
    /**
     * Thrown when the provided list of Account IDs is invalid
     */
    INVALID_ACCOUNTS: {
      name: `${prefix}INVALID_ACCOUNTS`,
      message: 'The provided accountIds contained invalid Account IDs. Account IDs must be strings.'
    },
    /**
     * Thrown when the provided Flag to retrieve is not a string
     */
    INVALID_FLAGS_KEY: {
      name: `${prefix}INVALID_FLAGS_KEY`,
      message: 'The Flag name provided is invalid. Flag names must be a valid key within the FeatureFlags enumeration module.'
    },
    /**
     * Thrown when the provided Object to search is not an Object
     */
    INVALID_FLAGS_OBJECT: {
      name: `${prefix}INVALID_FLAGS_OBJECT`,
      message: 'The Object provided for flags is invalid. Provide an Object with boolean properties and functions.'
    },
    /**
     * Thrown when the provided needle to search is nullish
     */
    INVALID_NEEDLE: {
      name: `${prefix}INVALID_NEEDLE`,
      message: 'The value provided for needle is nullish.'
    },
    /**
     * Thrown when the provided list of Role IDs is invalid
     */
    INVALID_ROLES: {
      name: `${prefix}INVALID_ROLES`,
      message: 'The provided roleIds contained invalid Role IDs. Role IDs must be integers.'
    },
    /**
     * Thrown when the provided list of User IDs is invalid
     */
    INVALID_USERS: {
      name: `${prefix}INVALID_USERS`,
      message: 'The provided userIds contained invalid User IDs. User IDs must be integers.'
    },
    /**
     * Thrown when the provided validator function is invalid
     */
    INVALID_VALIDATOR: {
      name: `${prefix}INVALID_VALIDATOR`,
      message: 'The provided validator is not a function. Provide a function which accepts an Array and returns a boolean.'
    },
    /**
     * Thrown when the provided validator error Object is invalid
     */
    INVALID_VALIDATOR_ERROR: {
      name: `${prefix}INVALID_VALIDATOR_ERROR`,
      message: 'The provided validator error is invalid. Provide an Object or Error instance.'
    },
    /**
     * Thrown when the value provided to one of the FeatureFlagManager's partials is not a function
     */
    NOT_A_FUNCTION: {
      name: `${prefix}NOT_A_FUNCTION`,
      message: 'The value provided to wrap in a feature flag is not a function.'
    }
  }
});
