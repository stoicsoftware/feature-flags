/**
 * FeatureFlags.js
 *
 * An enumeration module providing a set of Feature Flags as an Object; Flag can be accessed via
 * their key as any other Object property, and Flag values can either be primitive booleans or
 * functions which return booleans.
 *
 * @module FeatureFlags.js
 *
 * @copyright 2022 Stoic Software, LLC
 * @author Eric T Grubaugh <eric@stoic.software>
 * @license MIT
 *
 * @NApiVersion 2.1
 * @NModuleScope Public
 */
define([], () => (/** @alias module:FeatureFlags.js */ {
  EXAMPLE_TRUE: true,
  EXAMPLE_FALSE: false,
  get EXAMPLE_GETTER () { return true; },
}));
