/**
 * FeatureFlagCore.js
 *
 * Provides low-level methods for generating new types of Feature Flags. FeatureFlagManager is the
 * primary consumer of this module, using it to generate FeatureFlags for common use cases.
 *
 * @module FeatureFlagCore
 *
 * @NApiVersion 2.1
 *
 * @author Eric T Grubaugh <eric@stoic.software>
 * @copyright 2022 Stoic Software, LLC
 * @license MIT
 */
define([
  './enums/FeatureFlagErrors'
], (
  FeatureFlagErrors
) => {
  /**
   * Generic partial function for configuring a Feature Flag which searches for a single value
   * within a given list of values (e.g. an Account ID in a list of Accounts, a User ID in a list
   * of Users, etc).
   *
   * @partial
   *
   * @gov 0
   *
   * @param {function(*[]): boolean} validator A function that accepts an Array and ensures
   *  the elements of the Array is valid for the intended use of the Feature Flag; if this function
   *  returns false on a given list, the Feature Flag will throw at runtime.
   * @param {Object} validationError The error thrown when the validator function returns false
   * @param {*|function} needle Either the value to search in the list or a parameter-less
   *  function that executes at runtime and returns the value to search. The primary use case for
   *  providing a function here is to allow for retrieval of NetSuite module values (e.g. those from
   *  `N/runtime`) without causing an error. Comparison is done using Array.includes()
   *
   * @returns {function(*[], function(...[*]):*)} a new partial function that can be used to
   *  generate a Feature Flag that relies on searching for a single value within a list
   *
   * @throws {FeatureFlagError} validationError when the validator function returns false for the given list
   * @throws {FeatureFlagError} when fn is not a function
   */
  function forList(validator, validationError, needle) {
    if (typeof validator !== 'function') throw FeatureFlagErrors.INVALID_VALIDATOR;
    if (!validationError) throw FeatureFlagErrors.INVALID_VALIDATOR_ERROR;
    if (['null', 'undefined'].includes(typeof needle)) throw FeatureFlagErrors.INVALID_NEEDLE;

    return (haystack, fn) => {
      if (!validator(haystack)) throw validationError;
      if (typeof fn !== 'function') throw FeatureFlagErrors.NOT_A_FUNCTION;

      return (...args) => {
        const _needle = typeof needle === 'function' ? needle() : needle;

        if (!haystack.includes(_needle)) return;

        return fn(...args);
      };
    };
  }

  /**
   * Generic partial function for configuring a Feature Flag which searches an Object for a given
   * flag name; the flag name will be the property name within the given Object.
   *
   * @partial
   *
   * @gov 0
   *
   * @param {Object} flags An Object of key-value pairs representing Feature Flags. The keys may be
   *  any valid key name, while values may be any primitive boolean or a getter function which
   *  returns a primitive boolean.
   *
   * @returns {function(string, function(...[*]):*)} a new partial function that can be used to
   *  generate a Feature Flag that relies on searching for a specific Flag within the given Flags
   *  Object.
   *
   * @throws {FeatureFlagError} when flags is not an Object
   */
  function forObject(flags) {
    if (typeof flags !== 'object') throw FeatureFlagErrors.INVALID_FLAGS_OBJECT;

    return (flag, fn) => {
      if (!Object.keys(flags).includes(flag)) throw FeatureFlagErrors.INVALID_FLAGS_KEY;
      if (typeof fn !== 'function') throw FeatureFlagErrors.NOT_A_FUNCTION;

      return (...args) => {
        if (!flags?.[flag]) return;

        return fn(...args);
      };
    };
  }

  return /** @alias module:FeatureFlagCore */ {
    forList,
    forObject
  };
});
