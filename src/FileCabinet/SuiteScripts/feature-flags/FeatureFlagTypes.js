/**
 * FeatureFlagTypes.js
 *
 * typedefs for the Feature Flags modules
 *
 * @author Eric T Grubaugh <eric@stoic.software>
 * @copyright 2022 Stoic Software, LLC
 * @license MIT
 */

/**
 * Descriptor for errors thrown by the Feature Flags modules
 *
 * @typedef {Object} FeatureFlagError
 *
 * @property {string} name A name for the error
 * @property {string} message Details of the error
 */
