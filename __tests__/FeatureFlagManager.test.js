import FeatureFlagManager from '../src/FileCabinet/SuiteScripts/feature-flags/FeatureFlagManager';
import runtimeLib from 'N/runtime';

jest.mock('N/runtime',
  () => ({
    EnvType: {
      PRODUCTION: 'production',
      SANDBOX: 'sandbox'
    },
    getCurrentUser: () => ({
      id: 421
    })
  }),
  {virtual: true}
);

global.runtime = runtimeLib;

describe('FeatureFlagManager', () => {
  describe('nonProduction()', () => {
    it('throws when no function is given', () => {
      expect(() => FeatureFlagManager.nonProduction()).toThrow();
    });
    describe('when the environment is Production,', () => {
      beforeAll(() => {
        global.runtime.envType = global.runtime.EnvType.PRODUCTION;
      });
      it('does not call the flagged function', () => {
        const fn = jest.fn();
        const wrappedFn = FeatureFlagManager.nonProduction(fn);

        const actual = wrappedFn();

        expect(fn).not.toHaveBeenCalled();
        expect(actual).toBeUndefined();
      });
      afterAll(() => {
        global.runtime.envType = undefined;
      });
    });
    describe('when the environment is not Production', () => {
      beforeAll(() => {
        global.runtime.envType = global.runtime.EnvType.SANDBOX;
      });
      it('calls the flagged function', () => {
        const fn = jest.fn();
        const wrappedFn = FeatureFlagManager.nonProduction(fn);

        wrappedFn();

        expect(fn).toHaveBeenCalled();
      });
      it('returns the output of the flagged function', () => {
        const expected = 10;
        const fn = jest.fn(() => expected);
        const wrappedFn = FeatureFlagManager.nonProduction(fn);

        expect(wrappedFn()).toEqual(expected);
      });
      it('passes all given inputs to the flagged function', () => {
        const fn = (a, b) => a + b;
        const wrappedFn = FeatureFlagManager.nonProduction(fn);

        const expected = 7;
        const actual = wrappedFn(5, 2);

        expect(actual).toEqual(expected);
      });
      afterAll(() => {
        global.runtime.envType = undefined;
      });
    });
  });
  describe('flags are composable', () => {
    beforeAll(() => {
      global.runtime.accountId = '1234567';
    });
    it('does not call the wrapped function when all flag conditions are not met', () => {
      global.runtime.envType = global.runtime.EnvType.PRODUCTION;

      const expected = 10;
      const fn = jest.fn(() => expected);
      const wrappedFn = FeatureFlagManager.nonProduction(
        FeatureFlagManager.forAccounts(['1234567'], fn)
      );

      const actual = wrappedFn();

      expect(fn).not.toHaveBeenCalled();
      expect(actual).toBeUndefined();

      global.runtime.envType = undefined;
    });
    it('returns the output of the wrapped function when all flag conditions are met', () => {
      global.runtime.envType = global.runtime.EnvType.SANDBOX;

      const expected = 10;
      const fn = jest.fn(() => expected);
      const wrappedFn = FeatureFlagManager.nonProduction(
        FeatureFlagManager.forAccounts(['1234567'], fn)
      );

      const actual = wrappedFn();

      expect(fn).toHaveBeenCalled();
      expect(actual).toEqual(expected);

      global.runtime.envType = undefined;
    });
    afterAll(() => {
      global.runtime.accountId = undefined;
      global.runtime.envType = undefined;
    });
  });
});
