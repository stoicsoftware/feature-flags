/**
 * FeatureFlag_sl_harness.js
 *
 * A Suitelet which acts as a test harness for validating Feature Flag functionality within the
 * NetSuite UI.
 *
 * @module FeatureFlag_sl_harness.js
 *
 * @copyright 2022 Stoic Software, LLC
 * @author Eric T Grubaugh <eric@stoic.software>
 * @license MIT
 *
 * @NApiVersion 2.1
 * @NModuleScope Public
 * @NScriptType Suitelet
 */
define([
  'SuiteScripts/feature-flags/FeatureFlagManager'
], (
  FeatureFlagManager
) => {
  /**
   * Routes incoming requests to the appropriate handler based on request method.
   *
   * @gov XXX
   *
   * @param {Object} context Contains references to the incoming request and outgoing response
   *
   * @see [Help:onRequest]{@link https://system.netsuite.com/app/help/helpcenter.nl?fid=section_4407987288.html}
   */
  function onRequest(context) {
    log.audit({title: `${context.request.method} request received`});

    const eventRouter = {
      [https.Method.GET]: onGet
    };

    try {
      (eventRouter[context.request.method])(context);
    } catch (e) {
      onError({context, error: e});
    }

    log.audit({title: "Request complete."});
  }

  /**
   * Handles GET requests to the Suitelet. Renders the UI
   *
   * @gov XXX
   *
   * @param {Object} context Contains references to the incoming request and the outgoing response
   */
  function onGet(context) {
    // TODO UI and Client Script for executing various Feature Flags
  }

  /**
   * Handles errors which bubble up from the individual request handlers
   *
   * @gov 0
   *
   * @param {Object} params
   * @param {Error} params.error The error which triggered this handler
   * @param {Object} params.context Contains references to the incoming request and the outgoing response
   */
  function onError(params) {
    log.error({title: error?.name, details: params});
  }

  return /** @alias module:FeatureFlag_sl_harness.js */ {
    onRequest
  };
});
