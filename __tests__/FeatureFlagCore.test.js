import FeatureFlagCore from '../src/FileCabinet/SuiteScripts/feature-flags/FeatureFlagCore';

const FeatureFlagErrors = jest.requireActual('../src/FileCabinet/SuiteScripts/feature-flags/enums/FeatureFlagErrors');

describe('FeatureFlagCore', () => {
  describe('forList()', () => {
    it('throws when an invalid validator function is given', () => {
      expect(() => FeatureFlagCore.forList()).toThrow(FeatureFlagErrors.INVALID_VALIDATOR);
      expect(() => FeatureFlagCore.forList(5)).toThrow(FeatureFlagErrors.INVALID_VALIDATOR);
      expect(() => FeatureFlagCore.forList('abc')).toThrow(FeatureFlagErrors.INVALID_VALIDATOR);
    });
    it('throws when an invalid validationError is given', () => {
      expect(() => FeatureFlagCore.forList(() => true)).toThrow(FeatureFlagErrors.INVALID_VALIDATOR_ERROR);
    });
    it('throws when an invalid needle is given', () => {
      expect(() => FeatureFlagCore.forList(() => true, 'error')).toThrow(FeatureFlagErrors.INVALID_NEEDLE);
    });
    it('throws when an invalid haystack is given', () => {
      const err = 'error';

      const find10 = FeatureFlagCore.forList(() => false, err, 10);

      expect(() => find10([1,2,3], () => 37)).toThrow(err);
    });
    it('throws when no function is given to wrap', () => {
      const find10 = FeatureFlagCore.forList(() => true, FeatureFlagErrors.NOT_A_FUNCTION, 10);

      expect(() => find10(['a'])).toThrow(FeatureFlagErrors.NOT_A_FUNCTION);
      expect(() => find10(['a'], null)).toThrow(FeatureFlagErrors.NOT_A_FUNCTION);
      expect(() => find10(['a'], 'b')).toThrow(FeatureFlagErrors.NOT_A_FUNCTION);
    });
    it('does not call the wrapped function when needle is not found in haystack', () => {
      const fn = jest.fn(() => 10);
      const needle = 100;
      const haystack = [5, 6, 7];
      const findNeedle = FeatureFlagCore.forList(() => true, 'error', needle);
      const wrappedFn = findNeedle(haystack, fn);

      const actual = wrappedFn();

      expect(fn).not.toHaveBeenCalled();
      expect(actual).toBeUndefined();
    });
    it('returns the output of the wrapped function when needle is found in haystack', () => {
      const expected = 5;
      const fn = jest.fn(() => expected);
      const needle = 100;
      const haystack = [1, 10, 100, 1000, 10000];
      const findNeedle = FeatureFlagCore.forList(() => true, 'error', needle);
      const wrappedFn = findNeedle(haystack, fn);

      const actual = wrappedFn();

      expect(fn).toHaveBeenCalled();
      expect(actual).toBe(expected);
    });
  });
  describe('forObject()', () => {
    it('throws when an invalid Flags Object is given', () => {
      expect(() => FeatureFlagCore.forObject()).toThrow(FeatureFlagErrors.INVALID_FLAGS_OBJECT);
      expect(() => FeatureFlagCore.forObject(5)).toThrow(FeatureFlagErrors.INVALID_FLAGS_OBJECT);
      expect(() => FeatureFlagCore.forObject('abc')).toThrow(FeatureFlagErrors.INVALID_FLAGS_OBJECT);
    });
    it('throws when an invalid Flag name is given', () => {
      const flags = {
        FALSE_FLAG: false,
        TRUE_FLAG: true
      };

      const findFlag = FeatureFlagCore.forObject(flags);

      expect(() => findFlag()).toThrow(FeatureFlagErrors.INVALID_FLAGS_KEY);
      expect(() => findFlag(12345)).toThrow(FeatureFlagErrors.INVALID_FLAGS_KEY);
      expect(() => findFlag('NOT_A_REAL_FLAG')).toThrow(FeatureFlagErrors.INVALID_FLAGS_KEY);
    });
    it('throws when no function is given to wrap', () => {
      const haystack = {
        FALSE_FLAG: false,
        get TRUE_FLAG () { return true; }
      };

      const findFlag = FeatureFlagCore.forObject(haystack);

      expect(() => findFlag('FALSE_FLAG')).toThrow(FeatureFlagErrors.NOT_A_FUNCTION);
      expect(() => findFlag('TRUE_FLAG', null)).toThrow(FeatureFlagErrors.NOT_A_FUNCTION);
      expect(() => findFlag('FALSE_FLAG', 'b')).toThrow(FeatureFlagErrors.NOT_A_FUNCTION);
    });
    it('does not call the wrapped function when given flag is not truthy', () => {
      const expected = 5;
      const fn = jest.fn(() => expected);
      const flag = 'FALSE_FLAG';
      const haystack = {
        FALSE_FLAG: false,
        get TRUE_FLAG () { return true; }
      };
      const findFlag = FeatureFlagCore.forObject(haystack);
      const wrappedFn = findFlag(flag, fn);

      const actual = wrappedFn();

      expect(fn).not.toHaveBeenCalled();
      expect(actual).toBeUndefined();
    });
    it('returns the output of the wrapped function when given flag is truthy', () => {
      const expected = 5;
      const fn = jest.fn(() => expected);
      const flag = 'TRUE_FLAG';
      const flags = {
        FALSE_FLAG: false,
        get TRUE_FLAG () { return true; }
      };
      const findFlag = FeatureFlagCore.forObject(flags);
      const wrappedFn = findFlag(flag, fn);

      const actual = wrappedFn();

      expect(fn).toHaveBeenCalled();
      expect(actual).toBe(expected);
    });
  });
});
