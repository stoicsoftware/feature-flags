# SuiteScript Feature Flags

An opinionated utility module for hiding functionality behind feature flags within NetSuite's SuiteScript environment.

## Installation

TODO

## Usage

The core Feature Flags functionality lives in the `FeatureFlagManager` module. This module provides several partial
functions which cover common scenarios for preventing code from running until it has been deemed Production-ready.

TOOD Basic Example

`FeatureFlagManager` provides the following partials for implementing Feature Flags:

* [forAccounts(accountIds, fn)](#restrict-a-function-to-running-in-specific-netsuite-accounts) - Only run a given
  function in specified NetSuite Accounts
* [forRoles(users, fn)](#restrict-a-function-to-running-for-specific-roles) - Only run a given function for specified
  Roles
* [forUsers(users, fn)](#restrict-a-function-to-running-for-specific-users) - Only run a given function for specified
  Users
* [nonProduction(fn)](#prevent-a-function-from-running-in-a-production-environment) - Only run a given function in
  non-Production environments

Feature Flags built using these partials are composable. Each partial function outputs a new function, so the output of
one partial may be passed as the input to another. These compositions allow for higher complexity in your Feature Flag
conditions.

TODO Composition Example

Further details follow for each partial.

### Restrict a function to running in specific NetSuite Accounts

The `FeatureFlagManager` provides the `forAccounts` partial function, which ensures the wrapped function only runs in
specified NetSuite Accounts; the wrapped function will not execute for any Account ID which does not match a given list.

* The current Account ID is determined using the `N/runtime` module, comparing `runtime.accountId` to the given list of
  IDs.
* If the current Account ID does not match a listed Account ID, the wrapped function will not execute and will return
  `undefined`; ensure this is handled appropriately in the script(s) which invoke the wrapped function.

TODO Example

### Restrict a function to running for specific Roles

The `FeatureFlagManager` provides the `forRoles` partial function, which ensures the wrapped function only runs for
the specified Roles; the wrapped function will not execute for any Role which is not included in a given list.

* The current Role is determined using the `N/runtime` module, searching for `runtime.getCurrentUser().role` in the 
  given list of Role IDs.
* If the current Role does not match a listed Role ID, the wrapped function will not execute and will return
  `undefined`; ensure this is handled appropriately in the script(s) which invoke the wrapped function.

TODO Example

### Restrict a function to running for specific Users

The `FeatureFlagManager` provides the `forUsers` partial function, which ensures the wrapped function only runs for
the specified Users; the wrapped function will not execute for any User which is not included in a given list.

* The current User is determined using the `N/runtime` module, searching for `runtime.getCurrentUser().id` in the 
  given list of User IDs.
* If the current User does not match a listed User ID, the wrapped function will not execute and will return
  `undefined`; ensure this is handled appropriately in the script(s) which invoke the wrapped function.

TODO Example

### Prevent a function from running in a Production environment

The `FeatureFlagManager` provides the `nonProduction` partial function, which ensures the wrapped function does not run
in a Production environment; the wrapped function will still execute in all other environment types.

* The current environment type is determined using the `N/runtime` module, comparing `runtime.envType` to
  `runtime.EnvType.PRODUCTION`.
* If the environment type is Production, the wrapped function will always return `undefined`; ensure this is handled
  appropriately in the script(s) which invoke the wrapped function.

TODO Example

## Support

Contact eric@stoic.software

## Roadmap

See [Issues](https://gitlab.com/stoicsoftware/feature-flags/-/issues/) and
[Milestones](https://gitlab.com/stoicsoftware/feature-flags/-/milestones).

## Contributing

TODO

## Authors and acknowledgment

* Eric T Grubaugh, Stoic Software

## License

The SuiteScript Feature Flags application is available under the [MIT License](https://gitlab.com/stoicsoftware/feature-flags/-/blob/main/LICENSE).

